#include "common.h"

static SDL_Texture* pete[2];

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 1;

	pete[0] = loadTexture("gfx/pete01.png");
	pete[1] = loadTexture("gfx/pete02.png");

	player->texture = pete[0];

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;

		player->texture = pete[1];
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;

		player->texture = pete[0];
	}

	virtual void Landed(const FHitResult & Hit) override;

	int jumpCount;

	if (app.keyboard[SDL_SCANCODE_I] && player->isOnGround)
	{
		player->riding = NULL;

		player->dy = -20;

		playSound(SND_JUMP, CH_PLAYER);

	}

	if (app.keyboard[SDL_SCANCODE_II] && player->isOnGround)
	{
		player->riding = NULL;

		player->dy = -40;

		playerSound(SND_JUMP, CH_PLAYER);
	}
		
	jumpCount = 0;
	
	void player::Landed(const FHitResult & Hit)
	{
		Super::Landed(Hit);

		jumpCount = 0;
	}

	jumpCount++;
	if (jumpCount == 2)
	{
		Launchplayer(FVector(0, 0, 500), false, true);
	}


	if (app.keyboard[SDL_SCANCODE_SPACE])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
	}
}
