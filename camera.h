#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define STRNCPY(dest, src, n) strncpy(dest, src, n); dest[n - 1] = '\0'

#define PLATFORM_SPEED 4
#define SCREEN_WIDTH   2000
#define SCREEN_HEIGHT  1000

#define MAX_TILES    12

#define TILE_SIZE    128

#define MAP_WIDTH    80
#define MAP_HEIGHT   40

#define MAP_RENDER_WIDTH    40
#define MAP_RENDER_HEIGHT   24

#define PLAYER_MOVE_SPEED 6

#define MAX_NAME_LENGTH        32
#define MAX_LINE_LENGTH        1024
#define MAX_FILENAME_LENGTH    1024

#define MAX_KEYBOARD_KEYS 350

#define MAX_SND_CHANNELS 16

#define EF_NONE       0
#define EF_WEIGHTLESS (2 << 0)
#define EF_SOLID      (2 << 1)
#define EF_PUSH       (2 << 2)

#define GLYPH_WIDTH  18
#define GLYPH_HEIGHT 29

enum
{
	TEXT_LEFT,
	TEXT_CENTER,
	TEXT_RIGHT
};

enum
{
	SND_JUMP,
	SND_PIZZA,
	SND_PIZZA_DONE,
	SND_MAX
};

enum
{
	CH_PLAYER,
	CH_PIZZA
};
